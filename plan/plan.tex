\documentclass[10pt,a4paper,oneside]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage[left=3.5cm,right=3.5cm,top=3.5cm,bottom=3.5cm]{geometry}
\setlength{\parskip}{3mm}
\setlength{\parindent}{0cm}
\title{Investigating the Influence of Different Counting Environments in Count Matrix based Clone Detection}
\author{Lasse Schuirmann}
\newcommand*{\blankpage}{
  \vspace*{\fill}
  \begin{flushright}
  \tiny THIS PAGE INTENTIONALLY LEFT BLANK.
  \end{flushright}
  \pagebreak
}
\begin{document}
\maketitle

\pagebreak

\blankpage

\section*{About this Document}

This document is a planning document. It tries to summarize how a bachelor thesis with this topic could be realized and tries to define the topic specifically.

\section*{Introduction to CMCD}

Count Matrix based Clone Detection (CMCD) is an algorithm first proposed in 2011 by Yang Yuan and Yao Guo \cite{CMCD_initial}\cite{CMCD_followup} to detect code clones efficiently. It is designed to find cloned functions and it is not known that it was ever extended to find clones within functions.

It is particularly interesting because of its modular approach: the algorithm itself is generally applicable and thus language independent. However it depends on one language dependent component which generates language independent data.

Furthermore the authors claim (and more authors confirm \cite{CMCD_confirm}) that this approach delivers a very performant while superior code clone detection compared to other algorithms. There exist at least two different implementations of this algorithm \cite{CMCD_initial}\cite{CMCD_confirm}.

\subsection*{The Algorithm}

The CMCD algorithm is designed around one important assumption: The similarity of two variables can be measured by comparing how often they are used in specific ways, i.e. the identity of a variable can be approximately compared by comparing these uses. These "specific ways" are named "Counting Environments" (short CE) by the original authors. The CEs are a major subject of the thesis.

The counts of different uses of a variable within a function then make up a Count Vector (CV). By searching for functions which have similar CVs we can actually search for functions that probably have a similar behaviour. A big advantage of this procedure is that it is fully resistant against variable renamings and statement swapping.

\section*{Existing Work}

This thesis will build on the theory described above. In order to do a proper implementation for investigation, the freely available coala framework will be used \cite{coala_repo}. In order to ease parsing, clang may be used to generate an abstract syntax tree \cite{clang_proto}.

\section*{The Goal}

The goal of this thesis to investigate how different CEs affect the results of the code duplication detection routine. Namely measurable sizes are:

\begin{enumerate}
\item The Number of false positives.
\item The Number of known missed results.
\end{enumerate}

The former size is to be minimized while the latter is to be maximized.

\section*{Development Environment}

The code for this thesis will be developed in python and be publicly available on the coala main repository. (It will live on an own branch until it is finished.)

This allows seamless reuse of the coala testing, installation and translation infrastructure.

\section*{Work Plan}

This section provides detailed milestones to realize this thesis. Note that each week is not necessarily a week in real dates because the plan is stretched due to university courses.

Work on the thesis starts on April 13.

\subsection*{1st Week - Implementing Parsing (to April 26)}

This week will be used to get to know the algorithm better and implement parsing of the CVs.

Measurable Goals:

\begin{enumerate}
\item The parsing method is determined.
\item The target language(s) is/are determined.
\item Vector parsing is implemented for at least one language.
\item This includes the following CEs:
  \begin{enumerate}
  \item Assigning a variable
  \item Using variable for assignment
  \item Using a variable for a simple condition
  \item Using a variable for a loop condition
  \item Returning a variable value
  \end{enumerate}
\item (Optional) A prototype of the vector matching algorithm may exist.
\end{enumerate}

This includes a test suite.

\subsection*{2nd Week - Implementing Algorithm and Getting to Know it (to May 10)}

Measurable Goals:

\begin{enumerate}
\item The vector matching algorithm is implemented for arbitrary length vectors.
\item Minor modification possibilities on the algorithm are investigated.
\end{enumerate}

This includes a test suite.

\subsection*{3rd Week - Creating the Whole (to May 24)}

Hooking everything together and beginning the thesis.

Measurable Goals:

\begin{enumerate}
\item Sample code files for investigation are written \cite{samples}.
\item The whole algorithm works on those sample files.
\item A basic heuristic for discarding false positives does exist.
\item Add more CEs:
  \begin{enumerate}
  \item Add Scope to previous CEs
  \item Add arbitrary other CEs
  \end{enumerate}
\end{enumerate}

This includes a basic test suite.

\subsection*{4th Week - Investigating Single CEs (to May 31)}

Measurable Goals:

\begin{enumerate}
\item Writing of the thesis begins.
\item Every single CE is investigated against the sample code files. Results are written down in the thesis.
\item Couting Environments that work best on its own (i.e. mirroring the \textit{identity} of the variable best when being used without others) are determined.
\item CEs are investigated against at least one small real code base.
\end{enumerate}

\subsection*{5th Week - Investigating Combinations of CEs (to June 14)}

Measurable Goals:

\begin{enumerate}
\item Combinations of CEs are investigated against the sample code files. Results are written down in the thesis.
\item Results for single CEs are written down preliminarily.
\item An intermediate presentation is prepared.
\item An evaluation against predetermined results for the sample code files is done.
\item (Optional) Sample code files for investigation may be extended.
\end{enumerate}

\subsection*{Intermediate Presentation (maybe on June 11)}

The intermediate presentation will cover everything achieved by this date. It includes specifically:

\begin{enumerate}
\item An introduction to the topic.
\item An introduction to the algorithm.
\item A presentation of the investigation of the CEs so far.
\item An evaluation of the best quality achieved against predetermined results for the sample code files.
\end{enumerate}

\subsection*{6th Week - Testing on Real Code (to June 28)}

Measurable Goals:

\begin{enumerate}
\item The CEs are investigated against at least one real code base, preferably more of diverse sizes.
\item The results are written down preliminarily in the thesis.
\item The results are evaluated against one other code duplication detection tool if possible.
\item (Optional) The vectors might be weighted to achieve even better results.
\end{enumerate}

\subsection*{7th Week - Buffer (to July 12)}

Software related projects \textit{do} take longer then estimated because unforeseen problems \textit{will} occur. In the unlikely event that no buffer is needed more CEs or additional code bases can be investigated at this time to achieve a better result.

\subsection*{8th and 9th Week - Writing (to July 26)}

This weeks will be spend writing down missing parts and finalizing the thesis.

Measurable results:

\begin{enumerate}
\item Everything is written down.
\item The thesis is finished.
\end{enumerate}

\subsection*{After Finishing - The Greater Good}

The code written for this thesis will be reviewed and improved in public, and after several iterations be merged into the public main coala repository for the general good. It will be licensed under AGPL.

In case the thesis gets finished before August I may hold a talk about it on this years GUADEC in sweden, otherwise I may hold a talk about the disclosed parts (e.g. the working code) of the thesis at that time.

\begin{thebibliography}{999}
\bibitem {CMCD_initial} \url{http://sei.pku.edu.cn/~yaoguo/papers/Yuan-APSEC-11.pdf}
\bibitem {CMCD_followup} \url{http://sei.pku.edu.cn/~yaoguo/papers/Yuan-ASE-12.pdf}
\bibitem {CMCD_confirm} \url{http://crpit.com/confpapers/CRPITV147Chen.pdf}
\bibitem {coala_repo} \url{https://github.com/coala-analyzer/coala}
\bibitem {clang_proto} \url{https://github.com/coala-analyzer/clang-ast-bear}
\bibitem {samples} \url{http://research.cs.queensu.ca/home/cordy/Papers/RCK_SCP_Clones.pdf}
\end{thebibliography}
\end{document}